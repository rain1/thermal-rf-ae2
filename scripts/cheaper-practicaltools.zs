craftingTable.removeRecipe(<item:practicaltools:iron_hammer>);
craftingTable.addShaped("practicaltools:cheaper_iron_hammer", <item:practicaltools:iron_hammer>, [
    [<item:minecraft:iron_block>, <item:minecraft:stick>, <item:minecraft:iron_block>],
    [<item:minecraft:air>, <item:minecraft:stick>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:stick>, <item:minecraft:air>]
]);

craftingTable.removeRecipe(<item:practicaltools:golden_hammer>);
craftingTable.addShaped("practicaltools:cheaper_golden_hammer", <item:practicaltools:golden_hammer>, [
    [<item:minecraft:gold_block>, <item:minecraft:stick>, <item:minecraft:gold_block>],
    [<item:minecraft:air>, <item:minecraft:stick>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:stick>, <item:minecraft:air>]
]);

craftingTable.removeRecipe(<item:practicaltools:diamond_hammer>);
craftingTable.addShaped("practicaltools:cheaper_diamond_hammer", <item:practicaltools:diamond_hammer>, [
    [<item:minecraft:diamond_block>, <item:minecraft:stick>, <item:minecraft:diamond_block>],
    [<item:minecraft:air>, <item:minecraft:stick>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:stick>, <item:minecraft:air>]
]);

craftingTable.removeRecipe(<item:practicaltools:netherite_hammer>);
craftingTable.addShaped("practicaltools:cheaper_netherite_hammer", <item:practicaltools:netherite_hammer>, [
    [<item:minecraft:netherite_block>, <item:minecraft:stick>, <item:minecraft:netherite_block>],
    [<item:minecraft:air>, <item:minecraft:stick>, <item:minecraft:air>],
    [<item:minecraft:air>, <item:minecraft:stick>, <item:minecraft:air>]
]);
